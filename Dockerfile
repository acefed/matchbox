FROM denoland/deno:2.0.2 as builder

WORKDIR /app
COPY . .

RUN deno i -e index.ts

FROM denoland/deno:distroless-2.0.2

WORKDIR /app
COPY --from=builder /app .

ENV HOSTS=0.0.0.0
ENTRYPOINT ["deno", "-R=data,public", "-NE", "index.ts"]
