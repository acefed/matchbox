# Matchbox

- [1.0.0]
- [0.9.0] - 2024-03-10 - 8 files changed, 266 insertions(+), 162 deletions(-)
- [0.8.0] - 2024-02-04 - 12 files changed, 199 insertions(+), 143 deletions(-)
- [0.7.0] - 2023-12-17 - 10 files changed, 205 insertions(+), 147 deletions(-)
- [0.6.0] - 2023-11-24 - 19 files changed, 466 insertions(+), 313 deletions(-)
- [0.5.0] - 2023-03-19 - 14 files changed, 278 insertions(+), 185 deletions(-)
- [0.4.0] - 2022-12-15 - 12 files changed, 150 insertions(+), 33 deletions(-)
- [0.3.0] - 2022-10-30 - 11 files changed, 60 insertions(+), 38 deletions(-)
- [0.2.0] - 2022-08-13 - 14 files changed, 143 insertions(+), 51 deletions(-)
- 0.1.0 - 2022-07-20

[1.0.0]: https://gitlab.com/acefed/matchbox/-/compare/aee32468...main
[0.9.0]: https://gitlab.com/acefed/matchbox/-/compare/4b60c9db...aee32468
[0.8.0]: https://gitlab.com/acefed/matchbox/-/compare/234f3d4c...4b60c9db
[0.7.0]: https://gitlab.com/acefed/matchbox/-/compare/e14368f3...234f3d4c
[0.6.0]: https://gitlab.com/acefed/matchbox/-/compare/5baf4d1c...e14368f3
[0.5.0]: https://gitlab.com/acefed/matchbox/-/compare/9f9e7854...5baf4d1c
[0.4.0]: https://gitlab.com/acefed/matchbox/-/compare/89f8630f...9f9e7854
[0.3.0]: https://gitlab.com/acefed/matchbox/-/compare/d2ec82b5...89f8630f
[0.2.0]: https://gitlab.com/acefed/matchbox/-/compare/fb902463...d2ec82b5
