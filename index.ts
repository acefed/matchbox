import { Hono } from "hono";
import type { JSONValue } from "hono/utils/types";
import { basicAuth } from "hono/basic-auth";
import { cors } from "hono/cors";
import { etag } from "hono/etag";
import { serveStatic } from "hono/deno";

type JSONValueExt = JSONValue & {
  id: string;
  preferredUsername: string;
  inbox: string;
};

type JsonWebKeyExt = JsonWebKey & {
  kid?: string;
  x5c?: string[];
  x5t?: string;
  "x5t#S256"?: string;
  x5u?: string;
};

const ENV = {
  TLS_CA_CERT: Deno.env.get("TLS_CA_CERT"),
  TLS_CERT: Deno.env.get("TLS_CERT"),
  TLS_KEY: Deno.env.get("TLS_KEY"),
  HOSTS: Deno.env.get("HOSTS"),
  PORT: Deno.env.get("PORT"),
  CONFIG_JSON: Deno.env.get("CONFIG_JSON"),
  ENABLE_BASIC_AUTH: Deno.env.get("ENABLE_BASIC_AUTH"),
  BASIC_AUTH_USERNAME: Deno.env.get("BASIC_AUTH_USERNAME"),
  BASIC_AUTH_PASSWORD: Deno.env.get("BASIC_AUTH_PASSWORD"),
  SECRET: Deno.env.get("SECRET"),
  PRIVATE_KEY: Deno.env.get("PRIVATE_KEY"),
} as { [key: string]: string };
const enc = new TextEncoder();

let tlsCaCertPem, tlsCertPem, tlsKeyPem;
if (ENV.TLS_CA_CERT) {
  tlsCaCertPem = ENV.TLS_CA_CERT.split("\\n").join("\n");
  if (tlsCaCertPem.startsWith('"')) tlsCaCertPem = tlsCaCertPem.slice(1);
  if (tlsCaCertPem.endsWith('"')) tlsCaCertPem = tlsCaCertPem.slice(0, -1);
}
if (ENV.TLS_CERT) {
  tlsCertPem = ENV.TLS_CERT.split("\\n").join("\n");
  if (tlsCertPem.startsWith('"')) tlsCertPem = tlsCertPem.slice(1);
  if (tlsCertPem.endsWith('"')) tlsCertPem = tlsCertPem.slice(0, -1);
}
if (ENV.TLS_KEY) {
  tlsKeyPem = ENV.TLS_KEY.split("\\n").join("\n");
  if (tlsKeyPem.startsWith('"')) tlsKeyPem = tlsKeyPem.slice(1);
  if (tlsKeyPem.endsWith('"')) tlsKeyPem = tlsKeyPem.slice(0, -1);
}
const client = Deno.createHttpClient({ caCerts: [String(tlsCaCertPem ?? tlsCertPem ?? "")].filter(Boolean) });

let configJson;
if (ENV.CONFIG_JSON) {
  configJson = ENV.CONFIG_JSON;
  if (configJson.startsWith("'")) configJson = configJson.slice(1);
  if (configJson.endsWith("'")) configJson = configJson.slice(0, -1);
} else configJson = await Deno.readTextFile("data/config.json");
const CONFIG = JSON.parse(configJson);
const ME = [
  '<a href="https://',
  new URL(CONFIG.actor[0].me).hostname,
  '/" rel="me nofollow noopener noreferrer" target="_blank">',
  "https://",
  new URL(CONFIG.actor[0].me).hostname,
  "/",
  "</a>",
].join("");
const PRIVATE_KEY = await importPrivateKey(ENV.PRIVATE_KEY);
const PUBLIC_KEY = await privateKeyToPublicKey(PRIVATE_KEY);
const publicKeyPem = await exportPublicKey(PUBLIC_KEY);
const publicKeyJwk = await crypto.subtle.exportKey("jwk", PUBLIC_KEY) as JsonWebKeyExt;
delete publicKeyJwk.ext;
publicKeyJwk.use = "sig";

const app = new Hono();
app.use("*", async (c, next) => {
  await next();
  c.res.headers.append("Vary", "Accept, Accept-Encoding");
});
app.use("/u/:username", cors());
app.use("/.well-known/nodeinfo", cors());
app.use("/.well-known/webfinger", cors());
app.use("/users/:username", cors());
app.use("/user/:username", cors());
app.use("/:strRoot", async (c, next) => {
  if (c.req.param("strRoot").startsWith("@")) {
    const result = cors();
    return result(c, next);
  } else await next();
});
app.use("/nodeinfo/*", cors(), etag(), serveStatic({ root: "./public/" }));
app.use("/public/*", etag(), serveStatic({ root: "./public/" }));
app.use("/favicon.ico", etag(), serveStatic({ path: "./public/favicon.ico" }));
app.use("/humans.txt", etag(), serveStatic({ path: "./public/humans.txt" }));
app.use("/robots.txt", etag(), serveStatic({ path: "./public/robots.txt" }));
app.use("/s/:secret/u/:username", async (c, next) => {
  if (ENV.ENABLE_BASIC_AUTH === "true" && c.req.method === "POST") {
    const result = basicAuth({
      username: ENV.BASIC_AUTH_USERNAME,
      password: ENV.BASIC_AUTH_PASSWORD,
    });
    return result(c, next);
  } else await next();
});

function stob(s: string) {
  return Uint8Array.from(s, (c) => c.charCodeAt(0));
}

function btos(b: ArrayBuffer) {
  return String.fromCharCode(...new Uint8Array(b));
}

async function importPrivateKey(pem: string) {
  const header = "-----BEGIN PRIVATE KEY-----";
  const footer = "-----END PRIVATE KEY-----";
  let b64 = pem;
  b64 = b64.split("\\n").join("");
  b64 = b64.split("\n").join("");
  if (b64.startsWith('"')) b64 = b64.slice(1);
  if (b64.endsWith('"')) b64 = b64.slice(0, -1);
  if (b64.startsWith(header)) b64 = b64.slice(header.length);
  if (b64.endsWith(footer)) b64 = b64.slice(0, -1 * footer.length);
  const der = stob(atob(b64));
  const result = await crypto.subtle.importKey(
    "pkcs8",
    der,
    {
      name: "RSASSA-PKCS1-v1_5",
      hash: "SHA-256",
    },
    true,
    ["sign"],
  );
  return result;
}

async function privateKeyToPublicKey(key: CryptoKey) {
  const jwk = await crypto.subtle.exportKey("jwk", key);
  delete jwk.d;
  delete jwk.p;
  delete jwk.q;
  delete jwk.dp;
  delete jwk.dq;
  delete jwk.qi;
  delete jwk.oth;
  jwk.key_ops = ["verify"];
  const result = await crypto.subtle.importKey(
    "jwk",
    jwk,
    {
      name: "RSASSA-PKCS1-v1_5",
      hash: "SHA-256",
    },
    true,
    ["verify"],
  );
  return result;
}

async function exportPublicKey(key: CryptoKey) {
  const der = await crypto.subtle.exportKey("spki", key);
  let b64 = btoa(btos(der));
  let pem = "-----BEGIN PUBLIC KEY-----" + "\n";
  while (b64.length > 64) {
    pem += b64.substring(0, 64) + "\n";
    b64 = b64.substring(64);
  }
  pem += b64.substring(0, b64.length) + "\n";
  pem += "-----END PUBLIC KEY-----" + "\n";
  return pem;
}

function uuidv7() {
  const v = new Uint8Array(16);
  crypto.getRandomValues(v);
  const ts = BigInt(Date.now());
  v[0] = Number(ts >> 40n);
  v[1] = Number(ts >> 32n);
  v[2] = Number(ts >> 24n);
  v[3] = Number(ts >> 16n);
  v[4] = Number(ts >> 8n);
  v[5] = Number(ts);
  v[6] = v[6] & 0x0f | 0x70;
  v[8] = v[8] & 0x3f | 0x80;
  return Array.from(v, (b) => b.toString(16).padStart(2, "0")).join("");
}

function talkScript(req: string) {
  const ts = Date.now();
  if (new URL(req).hostname === "localhost") return `<p>${ts}</p>`;
  return [
    "<p>",
    '<a href="https://',
    new URL(req).hostname,
    '/" rel="nofollow noopener noreferrer" target="_blank">',
    new URL(req).hostname,
    "</a>",
    "</p>",
  ].join("");
}

async function getActivity(username: string, hostname: string, req: string) {
  const t = new Date().toUTCString();
  const sig = await crypto.subtle.sign(
    "RSASSA-PKCS1-v1_5",
    PRIVATE_KEY,
    stob(
      [
        `(request-target): get ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
      ].join("\n"),
    ),
  );
  const b64 = btoa(btos(sig));
  const headers = {
    Date: t,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/activity+json",
    "Accept-Encoding": "identity",
    "Cache-Control": "no-cache",
    "User-Agent": `Matchbox/1.0.0 (+https://${hostname}/)`,
  };
  const res = await fetch(req, { method: "GET", headers, client });
  const status = res.status;
  console.log(`GET ${req} ${status}`);
  const body = await res.json();
  return body;
}

async function postActivity(username: string, hostname: string, req: string, x: JSONValue) {
  const t = new Date().toUTCString();
  const body = JSON.stringify(x);
  const digest = await crypto.subtle.digest("SHA-256", enc.encode(body));
  const s256 = btoa(btos(digest));
  const sig = await crypto.subtle.sign(
    "RSASSA-PKCS1-v1_5",
    PRIVATE_KEY,
    stob(
      [
        `(request-target): post ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
        `digest: SHA-256=${s256}`,
      ].join("\n"),
    ),
  );
  const b64 = btoa(btos(sig));
  const headers = {
    Date: t,
    Digest: `SHA-256=${s256}`,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date digest"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/json",
    "Accept-Encoding": "gzip",
    "Cache-Control": "max-age=0",
    "Content-Type": "application/activity+json",
    "User-Agent": `Matchbox/1.0.0 (+https://${hostname}/)`,
  };
  console.log(`POST ${req} ${body}`);
  await fetch(req, { method: "POST", body, headers, client });
}

async function acceptFollow(username: string, hostname: string, x: JSONValueExt, y: JSONValue) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Accept",
    actor: `https://${hostname}/u/${username}`,
    object: y,
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function follow(username: string, hostname: string, x: JSONValueExt) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Follow",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function undoFollow(username: string, hostname: string, x: JSONValueExt) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Follow",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function like(username: string, hostname: string, x: JSONValueExt, y: JSONValueExt) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Like",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function undoLike(username: string, hostname: string, x: JSONValueExt, y: JSONValueExt) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Like",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function announce(username: string, hostname: string, x: JSONValueExt, y: JSONValueExt) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Announce",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: x.id,
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function undoAnnounce(username: string, hostname: string, x: JSONValueExt, y: JSONValueExt, z: string) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `${z}/activity`,
      type: "Announce",
      actor: `https://${hostname}/u/${username}`,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      object: x.id,
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function createNote(username: string, hostname: string, x: JSONValueExt, y: string) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript(y),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function createNoteImage(username: string, hostname: string, x: JSONValueExt, y: string, z: string) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript("https://localhost"),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      attachment: [
        {
          type: "Image",
          mediaType: z,
          url: y,
        },
      ],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function createNoteMention(username: string, hostname: string, x: JSONValueExt, y: JSONValueExt, z: string) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const at = `@${y.preferredUsername}@${new URL(y.inbox).hostname}`;
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      inReplyTo: x.id,
      content: talkScript(z),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      tag: [
        {
          type: "Mention",
          name: at,
        },
      ],
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function createNoteHashtag(username: string, hostname: string, x: JSONValueExt, y: string, z: string) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": ["https://www.w3.org/ns/activitystreams", { Hashtag: "as:Hashtag" }],
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript(y),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      tag: [
        {
          type: "Hashtag",
          name: `#${z}`,
        },
      ],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function updateNote(username: string, hostname: string, x: JSONValueExt, y: string) {
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const ts = Date.now();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `${y}#${ts}`,
    type: "Update",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: y,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript("https://localhost"),
      url: y,
      updated: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function deleteTombstone(username: string, hostname: string, x: JSONValueExt, y: string) {
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `${y}#Delete`,
    type: "Delete",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: y,
      type: "Tombstone",
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

app.get("/", (c) => c.text("Matchbox: ActivityPub@Hono"));

app.get("/about", (c) => c.text("About: Blank"));

app.get("/u/:username", (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  const acceptHeaderField = c.req.header("Accept");
  let hasType = false;
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  if (acceptHeaderField?.includes("application/activity+json")) hasType = true;
  if (acceptHeaderField?.includes("application/ld+json")) hasType = true;
  if (acceptHeaderField?.includes("application/json")) hasType = true;
  if (!hasType) {
    const body = `${username}: ${CONFIG.actor[0].name}`;
    const headers = { "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate` };
    return c.text(body, 200, headers);
  }
  const body = {
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
        schema: "https://schema.org/",
        PropertyValue: "schema:PropertyValue",
        value: "schema:value",
        Key: "sec:Key",
      },
    ],
    id: `https://${hostname}/u/${username}`,
    type: "Person",
    inbox: `https://${hostname}/u/${username}/inbox`,
    outbox: `https://${hostname}/u/${username}/outbox`,
    following: `https://${hostname}/u/${username}/following`,
    followers: `https://${hostname}/u/${username}/followers`,
    preferredUsername: username,
    name: CONFIG.actor[0].name,
    summary: "<p>0.9.0</p>",
    url: `https://${hostname}/u/${username}`,
    endpoints: { sharedInbox: `https://${hostname}/u/${username}/inbox` },
    attachment: [
      {
        type: "PropertyValue",
        name: "me",
        value: ME,
      },
    ],
    icon: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/${username}u.png`,
    },
    image: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/${username}s.png`,
    },
    publicKey: {
      id: `https://${hostname}/u/${username}#Key`,
      type: "Key",
      owner: `https://${hostname}/u/${username}`,
      publicKeyPem,
    },
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    "Content-Type": "application/activity+json",
  };
  return c.json(body, 200, headers);
});

app.get("/u/:username/jwk", (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  publicKeyJwk.kid = `https://${hostname}/u/${username}`;
  const headers = { "Content-Type": "application/jwk+json" };
  return c.json(publicKeyJwk, 200, headers);
});

app.get("/u/:username/inbox", (c) => c.body(null, 405));
app.post("/u/:username/inbox", async (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  const contentTypeHeaderField = c.req.header("Content-Type");
  let hasType = false;
  const y = await c.req.json();
  let t = y.type ?? "";
  const aid = y.id ?? "";
  const atype = y.type ?? "";
  if (aid.length > 1024 || atype.length > 64) return c.body(null, 400);
  console.log(`INBOX ${aid} ${atype}`);
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  if (contentTypeHeaderField?.includes("application/activity+json")) hasType = true;
  if (contentTypeHeaderField?.includes("application/ld+json")) hasType = true;
  if (contentTypeHeaderField?.includes("application/json")) hasType = true;
  if (!hasType) return c.body(null, 400);
  if (!c.req.header("Digest") || !c.req.header("Signature")) return c.body(null, 400);
  if (t === "Accept" || t === "Reject" || t === "Add") return c.body(null);
  if (t === "Remove" || t === "Like" || t === "Announce") return c.body(null);
  if (t === "Create" || t === "Update" || t === "Delete") return c.body(null);
  if (t === "Follow") {
    if (new URL(y.actor || "about:blank").protocol !== "https:") return c.body(null, 400);
    const x = await getActivity(username, hostname, y.actor);
    if (!x) return c.body(null, 500);
    await acceptFollow(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "Undo") {
    const z = y.object ?? {};
    t = z.type ?? "";
    if (t === "Accept" || t === "Like" || t === "Announce") return c.body(null);
    if (t === "Follow") {
      if (new URL(y.actor || "about:blank").protocol !== "https:") return c.body(null, 400);
      const x = await getActivity(username, hostname, y.actor);
      if (!x) return c.body(null, 500);
      await acceptFollow(username, hostname, x, z);
      return c.body(null);
    }
  }
  return c.body(null, 500);
});

app.post("/u/:username/outbox", (c) => c.body(null, 405));
app.get("/u/:username/outbox", (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/outbox`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  const headers = { "Content-Type": "application/activity+json" };
  return c.json(body, 200, headers);
});

app.get("/u/:username/following", (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/following`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  const headers = { "Content-Type": "application/activity+json" };
  return c.json(body, 200, headers);
});

app.get("/u/:username/followers", (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/followers`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  const headers = { "Content-Type": "application/activity+json" };
  return c.json(body, 200, headers);
});

app.post("/s/:secret/u/:username", async (c) => {
  const username = c.req.param("username");
  const hostname = new URL(CONFIG.origin).hostname;
  const send = await c.req.json();
  const t = send.type ?? "";
  if (username !== CONFIG.actor[0].preferredUsername) return c.notFound();
  if (!c.req.param("secret") || c.req.param("secret") === "-") return c.notFound();
  if (c.req.param("secret") !== ENV.SECRET) return c.notFound();
  if (new URL(send.id || "about:blank").protocol !== "https:") return c.body(null, 400);
  const x = await getActivity(username, hostname, send.id);
  if (!x) return c.body(null, 500);
  const aid = x.id ?? "";
  const atype = x.type ?? "";
  if (aid.length > 1024 || atype.length > 64) return c.body(null, 400);
  if (t === "follow") {
    await follow(username, hostname, x);
    return c.body(null);
  }
  if (t === "undo_follow") {
    await undoFollow(username, hostname, x);
    return c.body(null);
  }
  if (t === "like") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return c.body(null, 400);
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return c.body(null, 500);
    await like(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "undo_like") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return c.body(null, 400);
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return c.body(null, 500);
    await undoLike(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "announce") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return c.body(null, 400);
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return c.body(null, 500);
    await announce(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "undo_announce") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return c.body(null, 400);
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return c.body(null, 500);
    const z = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(z).protocol !== "https:") return c.body(null, 400);
    await undoAnnounce(username, hostname, x, y, z);
    return c.body(null);
  }
  if (t === "create_note") {
    const y = send.url || "https://localhost";
    if (new URL(y).protocol !== "https:") return c.body(null, 400);
    await createNote(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "create_note_image") {
    const y = send.url || `https://${hostname}/public/logo.png`;
    if (new URL(y).protocol !== "https:") return c.body(null, 400);
    if (new URL(y).hostname !== hostname) return c.body(null, 400);
    let z = "image/png";
    if (y.endsWith(".jpg") || y.endsWith(".jpeg")) z = "image/jpeg";
    if (y.endsWith(".svg")) z = "image/svg+xml";
    if (y.endsWith(".gif")) z = "image/gif";
    if (y.endsWith(".webp")) z = "image/webp";
    if (y.endsWith(".avif")) z = "image/avif";
    await createNoteImage(username, hostname, x, y, z);
    return c.body(null);
  }
  if (t === "create_note_mention") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return c.body(null, 400);
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return c.body(null, 500);
    const z = send.url || "https://localhost";
    if (new URL(z).protocol !== "https:") return c.body(null, 400);
    await createNoteMention(username, hostname, x, y, z);
    return c.body(null);
  }
  if (t === "create_note_hashtag") {
    const y = send.url || "https://localhost";
    if (new URL(y).protocol !== "https:") return c.body(null, 400);
    const z = send.tag || "Hashtag";
    await createNoteHashtag(username, hostname, x, y, z);
    return c.body(null);
  }
  if (t === "update_note") {
    const y = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(y).protocol !== "https:") return c.body(null, 400);
    await updateNote(username, hostname, x, y);
    return c.body(null);
  }
  if (t === "delete_tombstone") {
    const y = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(y).protocol !== "https:") return c.body(null, 400);
    await deleteTombstone(username, hostname, x, y);
    return c.body(null);
  }
  console.log(`TYPE ${aid} ${atype}`);
  return c.body(null);
});

app.get("/.well-known/nodeinfo", (c) => {
  const hostname = new URL(CONFIG.origin).hostname;
  const body = {
    links: [
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.0",
        href: `https://${hostname}/nodeinfo/2.0.json`,
      },
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.1",
        href: `https://${hostname}/nodeinfo/2.1.json`,
      },
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.2",
        href: `https://${hostname}/nodeinfo/2.2.json`,
      },
    ],
  };
  const headers = { "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate` };
  return c.json(body, 200, headers);
});

app.get("/.well-known/webfinger", (c) => {
  const username = CONFIG.actor[0].preferredUsername;
  const hostname = new URL(CONFIG.origin).hostname;
  const p443 = `https://${hostname}:443/`;
  let resource = c.req.query("resource");
  let hasResource = false;
  if (resource?.startsWith(p443)) resource = `https://${hostname}/${resource.slice(p443.length)}`;
  if (resource === `acct:${username}@${hostname}`) hasResource = true;
  if (resource === `mailto:${username}@${hostname}`) hasResource = true;
  if (resource === `https://${hostname}/@${username}`) hasResource = true;
  if (resource === `https://${hostname}/u/${username}`) hasResource = true;
  if (resource === `https://${hostname}/user/${username}`) hasResource = true;
  if (resource === `https://${hostname}/users/${username}`) hasResource = true;
  if (!hasResource) return c.notFound();
  const body = {
    subject: `acct:${username}@${hostname}`,
    aliases: [
      `mailto:${username}@${hostname}`,
      `https://${hostname}/@${username}`,
      `https://${hostname}/u/${username}`,
      `https://${hostname}/user/${username}`,
      `https://${hostname}/users/${username}`,
    ],
    links: [
      {
        rel: "self",
        type: "application/activity+json",
        href: `https://${hostname}/u/${username}`,
      },
      {
        rel: "http://webfinger.net/rel/avatar",
        type: "image/png",
        href: `https://${hostname}/public/${username}u.png`,
      },
      {
        rel: "http://webfinger.net/rel/profile-page",
        type: "text/plain",
        href: `https://${hostname}/u/${username}`,
      },
    ],
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    "Content-Type": "application/jrd+json",
  };
  return c.json(body, 200, headers);
});

app.get("/s", (c) => c.notFound());
app.get("/@", (c) => c.redirect("/"));
app.get("/u", (c) => c.redirect("/"));
app.get("/user", (c) => c.redirect("/"));
app.get("/users", (c) => c.redirect("/"));

app.get("/users/:username", (c) => c.redirect(`/u/${c.req.param("username")}`));
app.get("/user/:username", (c) => c.redirect(`/u/${c.req.param("username")}`));
app.get("/:strRoot", (c) => {
  if (!c.req.param("strRoot").startsWith("@")) return c.notFound();
  return c.redirect(`/u/${c.req.param("strRoot").slice(1)}`);
});

Deno.serve({
  hostname: ENV.HOSTS || "localhost",
  port: Number(ENV.PORT) || 8080,
  cert: tlsCertPem,
  key: tlsKeyPem,
}, app.fetch);
